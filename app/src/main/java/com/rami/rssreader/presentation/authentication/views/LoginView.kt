package com.rami.rssreader.presentation.authentication.views

interface LoginView {

    fun showError(message: String)

    fun navigateToSignUpScreen()

    fun showMessage(message: String)

    fun navigateToFeedsScreen()
}