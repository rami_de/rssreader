package com.rami.rssreader.presentation.authentication.presenters

import android.util.Log
import com.rami.rssreader.domain.interactors.AuthInteractor
import com.rami.rssreader.presentation.authentication.views.LoginView
import kotlinx.coroutines.*
import java.lang.Exception
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class LogInPresenter @Inject constructor(private val interactor: AuthInteractor): CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = job

    private lateinit var job: Job
    private lateinit var view: LoginView

    fun onSignUpButtonPressed() {
        view.navigateToSignUpScreen()
    }

    fun onLoginButtonPressed(username: String?, password: String?) {
        if (username.isNullOrEmpty() || password.isNullOrEmpty()) {
            view.showError("You must fill in both fields")
        } else {
            launch {
                val success = interactor.performLogin(username, password)
                withContext(Dispatchers.Main) {
                    if (success) {
                        Log.d("LoginPresenter", "Logged in")
                        view.navigateToFeedsScreen()
                    } else {
                        view.showError("Error in login")
                    }
                }
            }
        }
    }

    fun setView(view: LoginView) {
        job = Job()
        this.view = view
    }

    fun onPause() {
        job.cancel()
    }

    fun onMessageReceived(message: String?) {
        if (!message.isNullOrEmpty()) {
            view.showMessage(message)
        }
    }
}