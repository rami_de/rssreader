package com.rami.rssreader.presentation.feeds.presenters

import com.rami.rssreader.domain.entities.Feed
import com.rami.rssreader.domain.interactors.FeedsInteractor
import com.rami.rssreader.presentation.feeds.views.FeedsView
import kotlinx.coroutines.*
import java.lang.Exception
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class FeedsPresenter @Inject constructor(private val interactor: FeedsInteractor) : CoroutineScope {

    private lateinit var job: Job
    private lateinit var view: FeedsView

    override val coroutineContext: CoroutineContext
        get() = job

    fun setView(view: FeedsView) {
        this.view = view
        job = Job()
        loadFeeds()
    }

    fun onSwipedToRefresh() {
        loadFeeds()
    }

    fun onPause() {
        job.cancel()
    }

    fun loadFeeds() {
        launch {
            withContext(Dispatchers.Main) {
                view.showLoader()
            }
            val feeds = try {
                interactor.getFeeds()
            } catch (e: Exception) {
                view.showError(e.localizedMessage)
                e.printStackTrace()
                listOf<Feed>()
            }
            withContext(Dispatchers.Main) {
                view.hideLoader()
                view.showItems(feeds)
            }
        }
    }

    fun onAddFeedButtonTapped() {
        view.goToAddFeedScreen()
    }
}