package com.rami.rssreader.presentation.authentication.views

interface SignUpView {

    fun showError(message: String)

    fun navigateBackToLogInScreen(message: String)
}