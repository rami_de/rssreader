package com.rami.rssreader.presentation.feeds.views

import com.rami.rssreader.domain.entities.Feed

interface FeedsView {

    fun showItems(items: List<Feed>)

    fun showLoader()

    fun hideLoader()

    fun showError(message: String)

    fun goToAddFeedScreen()
}