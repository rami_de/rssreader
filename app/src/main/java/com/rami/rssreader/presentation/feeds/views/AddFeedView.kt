package com.rami.rssreader.presentation.feeds.views

interface AddFeedView {

    fun navigateBackToFeedsScreen()

    fun showError(message: String)
}