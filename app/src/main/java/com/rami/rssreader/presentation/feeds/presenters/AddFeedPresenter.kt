package com.rami.rssreader.presentation.feeds.presenters

import android.util.Log
import com.rami.rssreader.domain.interactors.FeedsInteractor
import com.rami.rssreader.presentation.feeds.views.AddFeedView
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class AddFeedPresenter @Inject constructor(private val interactor: FeedsInteractor) : CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = job

    private lateinit var job: Job
    private lateinit var view: AddFeedView

    fun onAddButtonPressed(url: String) {
        if (!url.isNullOrEmpty()) {
            launch {
                val success = interactor.addFeed(url)
                withContext(Dispatchers.Main) {
                    if (success) {
                        Log.d("FeedPresenter", "Feed added")
                        view.navigateBackToFeedsScreen()
                    } else {
                        view.showError("Error adding feed")
                    }
                }
            }
        }
    }

    fun setView(view: AddFeedView) {
        this.view = view
        job = Job()
    }

    fun onPause() {
        job.cancel()
    }
}