package com.rami.rssreader.presentation.authentication.presenters

import android.util.Log
import com.rami.rssreader.domain.interactors.AuthInteractor
import com.rami.rssreader.presentation.authentication.views.SignUpView
import kotlinx.coroutines.*
import java.lang.Exception
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class SignUpPresenter @Inject constructor(private val interactor: AuthInteractor): CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = job

    private lateinit var job: Job
    private lateinit var view: SignUpView

    fun onSignUpButtonPressed(username: String, password: String) {
        if (username.isNullOrEmpty() || password.isNullOrEmpty()) {
            view.showError("You must fill in both fields")
        } else {
            launch {
                val success = interactor.performSignUp(username, password)
                withContext(Dispatchers.Main) {
                    if (success) {
                        Log.d("Signup Presenter", "Registration successful")
                        view.navigateBackToLogInScreen("Registration successful")
                    } else {
                        view.showError("Error in registration")
                    }
                }
            }
        }
    }

    fun setView(view: SignUpView) {
        this.view = view
        job = Job()
    }

    fun onPause() {
        job.cancel()
    }

}