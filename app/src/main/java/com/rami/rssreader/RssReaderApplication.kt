package com.rami.rssreader

import android.app.Application
import com.rami.rssreader.di.AppComponent
import com.rami.rssreader.di.AppModule
import com.rami.rssreader.di.DaggerAppComponent

class RssReaderApplication : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
    }
}

