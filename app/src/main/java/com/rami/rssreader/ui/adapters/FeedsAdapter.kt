package com.rami.rssreader.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.rami.rssreader.R
import com.rami.rssreader.domain.entities.Feed
import kotlinx.android.synthetic.main.feeds_list_item.view.*
import java.util.ArrayList
import javax.inject.Inject

class FeedsAdapter @Inject constructor() : RecyclerView.Adapter<FeedsAdapter.FeedsViewHolder>(){
    private var feeds: List<Feed> = ArrayList()
    var navigationListener: FeedsNavigationListener? = null


    interface FeedsNavigationListener {
        fun onFeedSelected(id: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): FeedsViewHolder {
        return FeedsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.feeds_list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return feeds.size
    }

    override fun onBindViewHolder(holder: FeedsViewHolder, position: Int) {
        holder.bindItems(feeds[position], navigationListener)
    }

    fun updateFeeds(feeds: List<Feed>) {
        this.feeds = feeds
        notifyDataSetChanged()
    }


    class FeedsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bindItems(feed: Feed, navigationListener: FeedsNavigationListener?) {
            itemView.feed_title.text = feed.url
            itemView.feed_url.text = feed.url
            itemView.setOnClickListener { view ->
                navigationListener?.onFeedSelected(feed.id)
            }
        }
    }
}