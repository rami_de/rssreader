package com.rami.rssreader.ui.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import com.rami.rssreader.R
import com.rami.rssreader.RssReaderApplication
import com.rami.rssreader.presentation.authentication.presenters.SignUpPresenter
import com.rami.rssreader.presentation.authentication.views.SignUpView
import kotlinx.android.synthetic.main.signup_activity.*
import kotlinx.android.synthetic.main.signup_activity.et_password
import kotlinx.android.synthetic.main.signup_activity.et_username
import javax.inject.Inject

class SignUpActivity : AppCompatActivity(), SignUpView {

    @Inject
    lateinit var presenter: SignUpPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as RssReaderApplication).appComponent.inject(this)

        setContentView(R.layout.signup_activity)

        bt_register.setOnClickListener {
            presenter.onSignUpButtonPressed(et_username.text.toString(), et_password.text.toString())
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.setView(this)
    }

    override fun onPause() {
        presenter.onPause()
        super.onPause()
    }

    override fun showError(message: String) {
        Snackbar.make(signup_activity_container, message, Snackbar.LENGTH_LONG)
            .show()
    }

    override fun navigateBackToLogInScreen(message: String) {
        startActivity(Intent(this, LoginActivity::class.java)
            .putExtra("message", message))
        finish()
    }
}