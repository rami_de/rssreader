package com.rami.rssreader.ui.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.rami.rssreader.RssReaderApplication
import com.rami.rssreader.domain.interactors.AuthInteractor
import javax.inject.Inject

class EmptyActivity : AppCompatActivity() {


    @Inject
    lateinit var authInteractor: AuthInteractor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as RssReaderApplication).appComponent.inject(this)

        val activityIntent = if (authInteractor.isUserLoggedIn()) {
            Intent(this, MainActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        } else {
            Intent(this, LoginActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }
        startActivity(activityIntent)
        finish()
    }
}