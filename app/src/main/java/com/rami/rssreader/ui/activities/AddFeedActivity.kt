package com.rami.rssreader.ui.activities

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import com.rami.rssreader.R
import com.rami.rssreader.RssReaderApplication
import com.rami.rssreader.presentation.feeds.presenters.AddFeedPresenter
import com.rami.rssreader.presentation.feeds.views.AddFeedView
import kotlinx.android.synthetic.main.add_feed_activity.*
import javax.inject.Inject

class AddFeedActivity : AppCompatActivity(), AddFeedView {

    @Inject
    lateinit var presenter: AddFeedPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as RssReaderApplication).appComponent.inject(this)
        setContentView(R.layout.add_feed_activity)

        bt_add.setOnClickListener {
            presenter.onAddButtonPressed(et_url.text.toString())
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.setView(this)
    }

    override fun onPause() {
        presenter.onPause()
        super.onPause()
    }

    override fun navigateBackToFeedsScreen() {
        finish()
    }

    override fun showError(message: String) {
        Snackbar.make(add_feed_activity_container, message, Snackbar.LENGTH_LONG)
            .show()
    }
}