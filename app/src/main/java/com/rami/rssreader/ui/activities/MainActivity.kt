package com.rami.rssreader.ui.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.rami.rssreader.R
import com.rami.rssreader.RssReaderApplication
import com.rami.rssreader.domain.entities.Feed
import com.rami.rssreader.presentation.feeds.presenters.FeedsPresenter
import com.rami.rssreader.presentation.feeds.views.FeedsView
import com.rami.rssreader.ui.adapters.FeedsAdapter
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), FeedsView, FeedsAdapter.FeedsNavigationListener {

    @Inject
    lateinit var presenter: FeedsPresenter

    @Inject
    lateinit var adapter: FeedsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as RssReaderApplication).appComponent.inject(this)
        setContentView(R.layout.activity_main)

        feeds_recycler.layoutManager = LinearLayoutManager(applicationContext, RecyclerView.VERTICAL, false)
        feeds_recycler.adapter = adapter
        adapter.navigationListener = this

        swipe_to_refresh_feeds.setOnRefreshListener {
            presenter.onSwipedToRefresh()
        }

        fab.setOnClickListener {
            presenter.onAddFeedButtonTapped()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.setView(this)
    }

    override fun onPause() {
        presenter.onPause()
        super.onPause()
    }

    override fun showItems(items: List<Feed>) {
        adapter.updateFeeds(items)
    }

    override fun showLoader() {
        swipe_to_refresh_feeds.isRefreshing = true
    }

    override fun hideLoader() {
        swipe_to_refresh_feeds.isRefreshing = false
    }

    override fun showError(message: String) {
        Snackbar.make(rss_feeds_container, message, Snackbar.LENGTH_LONG)
            .show()
    }

    override fun onFeedSelected(id: Int) {
//        startActivity(
//            Intent(this, FeedDetailsActivity::class.java)
//                .putExtra(EXTRA_POST_ID, postId))
    }

    override fun goToAddFeedScreen() {
        startActivity(Intent(this, AddFeedActivity::class.java))
    }

}
