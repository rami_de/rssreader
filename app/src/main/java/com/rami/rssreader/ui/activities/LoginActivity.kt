package com.rami.rssreader.ui.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.rami.rssreader.R
import com.rami.rssreader.RssReaderApplication
import com.rami.rssreader.presentation.authentication.presenters.LogInPresenter
import com.rami.rssreader.presentation.authentication.views.LoginView
import kotlinx.android.synthetic.main.login_activity.*
import javax.inject.Inject

class LoginActivity : AppCompatActivity(), LoginView {

    @Inject
    lateinit var presenter: LogInPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as RssReaderApplication).appComponent.inject(this)

        setContentView(R.layout.login_activity)

        val bundle = intent.extras
        presenter.onMessageReceived(bundle?.getString("message"))

        tv_signup.setOnClickListener {
            presenter.onSignUpButtonPressed()
        }
        bt_login.setOnClickListener {
            presenter.onLoginButtonPressed(et_username.text.toString(), et_password.text.toString())
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.setView(this)
    }

    override fun onPause() {
        presenter.onPause()
        super.onPause()
    }

    override fun showError(message: String) {
        Snackbar.make(login_activity_container, message, Snackbar.LENGTH_LONG)
            .show()
    }

    override fun navigateToSignUpScreen() {
        startActivity(Intent(this, SignUpActivity::class.java))
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message,
            Toast.LENGTH_LONG).show()
    }

    override fun navigateToFeedsScreen() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}