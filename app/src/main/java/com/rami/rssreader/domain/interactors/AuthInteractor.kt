package com.rami.rssreader.domain.interactors

interface AuthInteractor {

    fun isUserLoggedIn(): Boolean

    suspend fun performLogin(userName: String, password: String): Boolean

    suspend fun performSignUp(userName: String, password: String): Boolean

}