package com.rami.rssreader.domain.interactors

import com.rami.rssreader.data.remote.repository.Repository
import com.rami.rssreader.domain.entities.Feed
import java.lang.Exception
import javax.inject.Inject

class FeedsInteractorImpl @Inject constructor(private val repository: Repository) : FeedsInteractor {

    override suspend fun getFeeds(): List<Feed> {
        return repository.getFeeds().await()
    }

    override suspend fun addFeed(url: String): Boolean {
        try {
            repository.addFeed(url).await()
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
        return true
    }
}