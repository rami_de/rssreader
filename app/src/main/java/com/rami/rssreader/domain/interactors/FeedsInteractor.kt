package com.rami.rssreader.domain.interactors

import com.rami.rssreader.domain.entities.Feed

interface FeedsInteractor {

    suspend fun getFeeds(): List<Feed>

    suspend fun addFeed(url: String): Boolean
}