package com.rami.rssreader.domain.interactors

import android.util.Log
import com.rami.rssreader.data.dto.LoginResponse
import com.rami.rssreader.data.dto.RegistrationResponse
import com.rami.rssreader.data.remote.repository.Repository
import retrofit2.HttpException
import java.lang.Exception
import javax.inject.Inject

class AuthInteractorImpl @Inject constructor(private val repository: Repository) : AuthInteractor {

    override fun isUserLoggedIn(): Boolean {
        return repository.isUserLoggedIn()
    }

    override suspend fun performLogin(userName: String, password: String): Boolean {
        val loginResponse: LoginResponse?
        try {
            loginResponse = repository.performLogin(userName, password).await()
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        } catch (e: HttpException) {
            e.printStackTrace()
            return false
        }
        repository.saveUserSession(loginResponse.userId, loginResponse.accessToken)
        Log.d("AuthInteractor", "user token: ${loginResponse.accessToken}")
        return true
    }

    override suspend fun performSignUp(userName: String, password: String): Boolean {
        val registrationResponse: RegistrationResponse?
        try {
            registrationResponse = repository.performSignUp(userName, password).await()
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        } catch (e: HttpException) {
            e.printStackTrace()
            return false
        }
        Log.d("AuthInteractor", "user token: ${registrationResponse.accessToken}")
        return true
    }

}