package com.rami.rssreader.domain.entities

data class Feed(val id: Int, val title: String, val url: String)