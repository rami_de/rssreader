package com.rami.rssreader.data.dto

data class FeedBody(private val url: String)