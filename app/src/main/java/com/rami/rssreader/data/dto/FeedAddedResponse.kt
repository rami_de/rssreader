package com.rami.rssreader.data.dto

data class FeedAddedResponse(val id: Int, val title: String, val url: String)