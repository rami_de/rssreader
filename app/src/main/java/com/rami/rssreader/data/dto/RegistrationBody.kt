package com.rami.rssreader.data.dto

data class RegistrationBody(private val user: String, private val password: String)