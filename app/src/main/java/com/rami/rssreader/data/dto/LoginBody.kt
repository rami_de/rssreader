package com.rami.rssreader.data.dto

data class LoginBody(private val user: String, private val password: String)