package com.rami.rssreader.data.remote.client

import com.rami.rssreader.data.dto.*
import com.rami.rssreader.domain.entities.Feed
import kotlinx.coroutines.Deferred
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiClient {

    @POST("/users/register")
    fun register(@Body body: RegistrationBody): Deferred<RegistrationResponse>

    @POST("/users/login")
    fun login(@Body body: LoginBody): Deferred<LoginResponse>

    @POST("/feeds/add")
    fun addFeed(@Body body: FeedBody): Deferred<FeedAddedResponse>

    @GET("/feeds")
    fun getFeeds(): Deferred<List<Feed>>

}