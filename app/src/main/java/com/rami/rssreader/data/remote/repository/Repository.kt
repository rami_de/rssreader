package com.rami.rssreader.data.remote.repository

import com.rami.rssreader.data.dto.FeedAddedResponse
import com.rami.rssreader.data.dto.LoginResponse
import com.rami.rssreader.data.dto.RegistrationResponse
import com.rami.rssreader.domain.entities.Feed
import kotlinx.coroutines.Deferred

interface Repository {

    fun isUserLoggedIn(): Boolean

    suspend fun performLogin(username: String, password: String): Deferred<LoginResponse>

    suspend fun performSignUp(username: String, password: String): Deferred<RegistrationResponse>

    fun saveUserSession(userId: Int, token: String)

    suspend fun getFeeds(): Deferred<List<Feed>>

    suspend fun addFeed(url: String): Deferred<FeedAddedResponse>
}