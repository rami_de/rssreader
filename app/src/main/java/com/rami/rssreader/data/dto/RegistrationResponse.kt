package com.rami.rssreader.data.dto

import com.google.gson.annotations.SerializedName

data class RegistrationResponse(@SerializedName("access_token") val accessToken: String,
                                @SerializedName("user_id") val userId: Int)