package com.rami.rssreader.data.remote.repository

import android.content.SharedPreferences
import com.rami.rssreader.data.dto.*
import com.rami.rssreader.data.remote.client.ApiClient
import com.rami.rssreader.domain.entities.Feed
import kotlinx.coroutines.Deferred
import javax.inject.Inject

class RepositoryImpl @Inject constructor(private val apiClient: ApiClient,
                                         private val sharedPreferences: SharedPreferences): Repository {

    override fun isUserLoggedIn(): Boolean {
        val userId = sharedPreferences.getInt("userId", -1)
        val token: String? = sharedPreferences.getString("token",  null)
        return (userId != -1 && !token.isNullOrEmpty())
    }

    override suspend fun performLogin(username: String, password: String): Deferred<LoginResponse> {
        return apiClient.login(LoginBody(username, password))
    }

    override suspend fun performSignUp(username: String, password: String): Deferred<RegistrationResponse> {
        return apiClient.register(RegistrationBody(username, password))
    }

    override fun saveUserSession(userId: Int, token: String) {
        with(sharedPreferences.edit()) {
            putString("token", token)
            putInt("userId", userId)
            apply()
        }
    }

    override suspend fun getFeeds(): Deferred<List<Feed>> {
        return apiClient.getFeeds()
    }

    override suspend fun addFeed(url: String): Deferred<FeedAddedResponse> {
        return apiClient.addFeed(FeedBody(url))
    }

}