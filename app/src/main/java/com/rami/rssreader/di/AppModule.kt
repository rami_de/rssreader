package com.rami.rssreader.di

import android.content.Context
import android.content.SharedPreferences
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.rami.rssreader.RssReaderApplication
import com.rami.rssreader.data.remote.client.ApiClient
import com.rami.rssreader.data.remote.repository.Repository
import com.rami.rssreader.data.remote.repository.RepositoryImpl
import com.rami.rssreader.domain.interactors.AuthInteractor
import com.rami.rssreader.domain.interactors.AuthInteractorImpl
import com.rami.rssreader.domain.interactors.FeedsInteractor
import com.rami.rssreader.domain.interactors.FeedsInteractorImpl
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule(private val application: RssReaderApplication) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application.applicationContext

    @Provides
    @Singleton
    fun providesRetrofit(client: OkHttpClient): ApiClient {
        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .baseUrl("http://167.99.162.146/")
            .client(client)
            .build()

        return retrofit.create(ApiClient::class.java)
    }

    @Provides
    @Singleton
    fun providesOkHttpClient(sharedPreferences: SharedPreferences): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient().newBuilder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor { chain ->
                var request = chain.request()
                val token = sharedPreferences.getString("token", null)
                request = request.newBuilder()
                    .header("Authorization", "Bearer $token")
                    .build()
                chain.proceed(request)
            }
            .build()
    }

    @Provides
    @Singleton
    fun providesSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences("MySharedPrefs", Context.MODE_PRIVATE)
    }

    @Singleton
    @Provides
    fun provideAuthInteractor(repository: Repository): AuthInteractor {
        return AuthInteractorImpl(repository)
    }

    @Provides
    @Singleton
    fun provideRepository(apiClient: ApiClient, sharedPreferences: SharedPreferences): Repository {
        return RepositoryImpl(apiClient, sharedPreferences)
    }

    @Singleton
    @Provides
    fun provideFeedsInteractor(repository: Repository): FeedsInteractor {
        return FeedsInteractorImpl(repository)
    }

}
