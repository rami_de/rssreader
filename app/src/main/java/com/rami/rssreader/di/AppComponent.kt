package com.rami.rssreader.di

import com.rami.rssreader.RssReaderApplication
import com.rami.rssreader.ui.activities.*
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(application: RssReaderApplication)

    fun inject(activity: LoginActivity)

    fun inject(activity: MainActivity)

    fun inject(activity: SignUpActivity)

    fun inject(activity: EmptyActivity)

    fun inject(activity: AddFeedActivity)
}